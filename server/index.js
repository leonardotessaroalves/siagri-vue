const jsonServer = require('json-server')
const server = jsonServer.create()
const path = require('path')
const router = jsonServer.router(path.join(__dirname, 'db.json'))
const middlewares = jsonServer.defaults()

server.use(middlewares)

var fs = require('fs')

var routes = JSON.parse(fs.readFileSync(path.join(__dirname, 'routes.json')))

server.use(jsonServer.bodyParser)

server.use(jsonServer.rewriter(routes))

server.use(router)

server.listen(3001, () => console.log('<<<<<<<<<<<SERVIDOR RODANDO - leonardotessaroalves@gmail.com >>>>>>>>>>>>>>'))
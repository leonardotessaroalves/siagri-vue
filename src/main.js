// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/App'
import Mainconfig from '@/core/config'
import { sync } from 'vuex-router-sync'
import router from '@/core/router'
import store from '@/core/store'

const unsync = sync(store, router)
unsync()

// Vue.config.performance = true
Object.assign(Vue.config, Mainconfig)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

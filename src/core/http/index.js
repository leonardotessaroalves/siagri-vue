import axios from 'axios'
// import { Events } from '@/core/http/events'

const config = {
  base: 'http://localhost:3001/api'
}

const CLIENTE = axios.create({
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})

CLIENTE.defaults.baseURL = config.base

axios.defaults.headers.post['Content-Type'] = 'application/json'

CLIENTE.interceptors.request.use((config) => {
  // Events.$emit('before-request')
  return config
}, (error) => {
  // Events.$emit('request-error')
  return Promise.reject(error)
})

// Add a response interceptor
CLIENTE.interceptors.response.use((config) => {
  // Events.$emit('after-response')
  return config
}, (error) => {
  // Events.$emit('response-error')
  return Promise.reject(error.response.data)
})

export default CLIENTE

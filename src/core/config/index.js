import Vue from 'vue'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'
import * as VueGoogleMaps from 'vue2-google-maps'

// PLUGINS
Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDuguDdioanqd75MTE7REm9UmGvY4kPHn0',
    libraries: 'places'
  },
  autobindAllEvents: false
})

const config = {
  productionTip: false,
  performance: true
}

export default config

import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/core/views/home'
import _ from 'lodash'
import http from '@/core/http'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home,
      children: []
    }
  ]
})

http.get('routes').then((res) => {
  _.each(res.data, (obj) => {
    router.addRoutes([{
      path: `${obj.path}`,
      component: () => import('@/' + obj.url)
    }])
  })
})

export default router

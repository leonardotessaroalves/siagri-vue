import Vuex from 'vuex'
import modules from '@/core/store/modules'

export default new Vuex.Store({
  modules
})

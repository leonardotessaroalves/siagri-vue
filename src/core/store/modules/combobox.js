import http from '@/core/http'

const state = {
  token: undefined
}

const actions = {
  async login ({commit}, payload) {
    await http.post('/users/auth', JSON.stringify(payload)).then(user => {
      localStorage.setItem('token', JSON.stringify(user.data.token))
      commit('LOGIN_SUCCESS', user.data.token)
      return Promise.resolve(user.data)
    },
    error => Promise.reject(error))
  },
  async logout ({commit}, payload) {
    localStorage.removeItem('token')
  }
}

const mutations = {
  'LOGIN_SUCCESS' (state, e) {
    state.token = e
  }
}

const getters = {}

export default {
  state,
  actions,
  getters,
  mutations
}

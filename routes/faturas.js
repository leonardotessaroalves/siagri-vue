const low = require('lowdb');

const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');

const db = low(adapter);

let _ = require('lodash');

exports.setup = (server) => {
  
  /**GET*/
  server.get('/faturas', (req, res) => {    
        
    if (_.isEmpty(req.query) && req.method === 'GET') {
      let data = db.get("faturas")
      return res.send(data)
    }    
    
    let data = db.get("faturas").find({ idfatura: req.query.idfatura })

    return res.send(data)
  })

  /**POST*/
  server.post('/faturas', (req, res) => {
    
    let data = db.get("faturas")

    let index = 0

    if (data.last().value().idfatura) {
      index = Number(data.last().value().idfatura)
    }

    let datacount = String(index + 1)

    let args = {
      idfatura: datacount,
      idusuario: req.body.idusuario || '',
      nome_empresa: req.body.nome_empresa || '',
      valor: req.body.valor || '',
      data_vencimento: req.body.data_vencimento || '',
      pagou: req.body.pagou || ''
    }
      
    data.push(args).write()
    
    res.send(args);

  })

  /**DELETE*/
  server.delete('/faturas', (req, res) => {
    
    let data = db.get("faturas")
    
    if (req.query.idfatura) {
      data.remove({ idfatura: req.query.idfatura }).write()
    }

    res.send({});
    
  })

  /**UPDATE*/
  server.put('/faturas', (req, res) => {
    
    let data = db.get("faturas")

    let args = {
      idusuario: req.body.idusuario || '',
      nome_empresa: req.body.nome_empresa || '',
      valor: req.body.valor || '',
      data_vencimento: req.body.data_vencimento || '',
      pagou: req.body.pagou || ''
    }
    
    if (req.body.idfatura) {
      data.find({ idfatura: req.body.idfatura })
      .assign({...args}).write()
    }

    res.send({});
    
  })

}